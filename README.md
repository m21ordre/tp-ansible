# Commandes vagrant
`vagrant up`: monter toutes les VM définies dans le `Vagrantfile`.\
`vagrant destroy`: détruire toutes les VM définies dans le `Vagrantfile`.\
`vagrant up <args>`: monter la ou les VMs spécifiées.
`vagrant destroy <args>`: détruire la ou les VMs spécifiées.
